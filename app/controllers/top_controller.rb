class TopController < ApplicationController
  require 'mysql2'
  attr_accessor :client

  #client.query("select name from class_name;").each do | col1 |
  #  p col1["name"]
  #end

  def index
    client = SqlHelper.createDB()
    @comments = client.query("SELECT * FROM comment ORDER BY id DESC;")
    @comments.each do |row|
      puts row
    end
  
  end


  def text_create 
    @client = Mysql2::Client.new(host: "localhost", username: "vagrant", password: 'password')
    @client.query("USE SQL_TRY;")
    records = @client.query("SELECT * FROM comment;").count
    id = records + 1 
    name = params["name"]
    content = params["content"]
    @client.query("INSERT INTO comment (id,name,content) VALUES(#{id},'#{name}','#{content}');")
    redirect_to action: 'index'
  end
end
