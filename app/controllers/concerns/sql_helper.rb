module SqlHelper
    require 'mysql2'
    def createDB
        client = Mysql2::Client.new(host: "localhost", username: "vagrant", password: 'password')
        client.query("CREATE DATABASE IF NOT EXISTS SQL_TRY;")
        client.query("USE SQL_TRY;")
        client.query("CREATE TABLE IF NOT EXISTS comment( \
        id      int PRIMARY KEY, \
        name    varchar(128), \
        content   varchar(256) \
        );" )
        client
    end
    module_function :createDB
end
