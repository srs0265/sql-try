# README

SQLの学習用に掲示板を作成(for Ubuntu 18.04)

以下のコマンドを実行

    sudo apt-get install mysql-server mysql-client

mysqlのインストール完了したら、アカウントを作成しておく

下記と違うものにする場合はソースコード(topコントローラーなど）の書き換えが必要

    ホスト：localhost
    ユーザー名: vagrant
    パスワード: password

以下のコマンドを実行

    asdf install(asdfは事前にインストールしておくこと)
    bundle install

サーバーを起動(bオプションは仮想OSで立ち上げて、ホストOSからアクセスする場合に必要)
    
    rails s -b 0.0.0.0


備考：mysqlのgemはmysql2を使用する。（mysqlは最新のrubyでは使えない)


